% The Constitution of the Graduate Physics Student Association at the University
% of Alberta.
%
% Edited by Burkhard Ritter <burkhard@ualberta.ca>, March 2013. A big thank you
% to Daniel Laycock, Colin More and Roshan Achal for help, feedback and support.
%
%
% Suggestions for future changes:
%   * "Approval Process" section should be more specific, e.g. what is required
%     for the Council to approve something (50% ?)
%   * mechanism to automatically remove inactive Council members (e.g. officers
%     who do not attend Council meetings five times in a row loose their
%     position)
%
% Suggestions from Logan (March 21, 2013):
%   * Section V, GPSA Cocunil: Something that's never been in here is how many
%     terms someone may stay in a particular position. How many terms may
%     someone remain on council in any position? Do you want to consider having
%     a position that is for more than 1 year?
%   * V.H	Financial Review: "A financial report will be submitted to Council
%     prior to its succession." -- But, it has already been succeeded by the
%     time the report is due.
%   * VI.E Voting: What about voting by council? This has never been defined in
%     the constitution. i.e. When does council need to vote on something? There
%     is quorum for council votes defined above, but no reason to require it!
%     (Edit: While I agree, I think this should go somewhere into section V.
%     E.g. into the "Approval process" section. Burkhard.)


\documentclass[11pt]{article}
\usepackage{hyperref}
\usepackage{titlesec}

% Custom format and numbering for sections and enumerated items
\titleformat{\section}{\large\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}{\large}{\thesubsection}{1em}{}
\renewcommand{\thesection}{\Roman{section}}
\renewcommand{\thesubsection}{\thesection.\Alph{subsection}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\roman{enumii}}

\begin{document}

% Title
\title{
  The Constitution of the University of Alberta Graduate Physics Student
  Association
  }
\author{}
\date{}
\maketitle



\section*{Document history}
Revision 1.4 March 2013, June 2013\\
Revision 1.3 Draft 02 October 2011\\
Revision 1.2 Draft 01 March 2011\\
Revision 1.1 Draft 02 March 2010\\
Revision 0.3 Draft 24 April 2009\\
Revision 0.2 Draft 06 Sept 2007\\
Revision 0.1 Draft 25 March 2007



\section{Name}
The name of the organization shall be the ``University of Alberta Graduate
Physics Student Association'', hereinafter referred to as the GPSA.



\section{Definitions}
\begin{enumerate}
  \item ``Constitution'' refers to the Constitution of the GPSA;
  \item ``Council'' means the Council of the GPSA;
  \item ``Mailing List'' refers to the graduate physics student mailing list at
    the University of Alberta, with the email address\\
    \href{mailto:phys-grads@mailman.srv.ualberta.ca}{phys-grads@mailman.srv.ualberta.ca};
  \item ``Website'' refers to the GPSA website at\\
    \url{http://gpsa.physics.ualberta.ca/};
  \item ``Facebook Page'' refers to the GPSA Facebook page at\\
    \url{https://www.facebook.com/physgpsa}; and
  \item ``Bearsden Page'' refers to the GPSA Bearsden page at\\
    \url{https://alberta.collegiatelink.net/organization/physgpsa}.
\end{enumerate}



\section{Membership}
\subsection{Levels of Membership}
\label{sec:Levels of Membership}
\begin{enumerate}
  \item All graduate students in good standing enrolled in the Department of
    Physics at the University of Alberta are full members; and
  \item University of Alberta staff may become associate members, for a given
    calendar year, upon approval of Council.
\end{enumerate}

\subsection{Privileges of full members}
Full members are entitled to:
\begin{enumerate}
  \item attend general meetings and GPSA events;
  \item vote during the election of Council;
  \item run for an officer position on Council; and
  \item the right to be heard and represented by Council.
\end{enumerate}
Associate members are entitled to:
\begin{enumerate}
  \item be informed of and attend general meetings and GPSA events, albeit without voting privileges or other preferential status.
\end{enumerate}

\subsection{Membership Dues and Obligations}
No financial dues are required for membership. The GPSA has no financial
obligations towards its members.



\section{Mandate}
The primary objectives of the GPSA are to:
\begin{enumerate}
  \item provide representation of members with other relevant parties; and
  \item promote the general welfare of members.
\end{enumerate}



\section{GPSA Council}
Council is comprised of the elected officers outlined in
section~\ref{sec:Elected Officers}. Specific responsibilities of each elected
officer are outlined therein, though it is expected that each elected officer
will assist with the general running of the GPSA, as required, and will make all
reasonable efforts to attend Council Meetings. No person will hold more than one
position outlined in \ref{sec:Elected Officers}~a-f; however, the focus area
representatives, as outlined in \ref{sec:Elected Officers}~g, may additionally
hold no more than one of the positions outlined in \ref{sec:Elected
Officers}~a-f.

\subsection{Elected Officers}
\label{sec:Elected Officers}
The elected officers of the GPSA, in order of precedence, are:
\begin{enumerate}
  \item President\\
    It is the responsibility of the President to:
    \begin{enumerate}
      \item oversee the general affairs of the GPSA and ensure the fulfilment of
        its mandate; 
      \item chair all meetings of the GPSA; 
      \item engage in activities to raise funds for the GPSA; 
      \item act as a signing authority for the bank account;
      \item sit on the Department of Physics Council;
      \item organize and oversee elections and voting, as outlined in
        sections \ref{sec:Election of Council} and section~\ref{sec:Voting}; and
      \item in case of absence, appoint one of the Vice Presidents as an
        alternate who will temporarily assume all responsibilities of the
        President.
    \end{enumerate}
  \item Vice President Academic\\
    It is the goal of the Vice President Academic to identify and develop
    opportunities to foster the scientific environment for graduate students in 
    the Department of Physics and to encourage the exchange of knowledge. It is this
    officer's responsibility to plan and organize academic events for members or
    persons external to the GPSA, which may include, but is not limited to,
    organizing the ``Annual Symposium on Graduate Physics Research at the
    University of Alberta'' which typically takes place in September, inviting
    speakers to the Department of Physics, organizing career events, organizing
    journal clubs, planning outreach activities, and organizing other
    educational workshops. Additionally, it is this officer's responsibility to
    sit on the Department of Physics Council, or select an alternate to do so.
  \item Vice President Student Life\\
    It is the goal of the Vice President Student Life to create a vibrant
    graduate physics student community with a sense of community spirit and a
    distinct identity. It is this officer's responsibility to plan and organize
    social events for members, or the whole physics community at the University
    of Alberta, which may include, but is not limited to, organizing parties,
    movie nights, barbecues, trips, sporting events, and coffee socials.
  \item Vice President Finance\\
    It is the responsibility of the Vice President Finance to:
    \begin{enumerate}
      \item oversee the finances of the GPSA;
      \item manage the bank account of the GPSA; 
      \item act as a signing authority for the bank account;
      \item appoint an additional Vice President as signing authority for the
        bank account, if necessary;
      \item support the President's fundraising activities; and
      \item submit a review of the GPSA's finances, as outlined in
        section~\ref{sec:Financial Review}.
    \end{enumerate}
  \item Vice President Communication\\
    It is the responsibility of the Vice President Communication to:
    \begin{enumerate}
      \item manage the GPSA's communication, both internally and externally,
        which may include, but is not limited to, writing email announcements
        and correspondence with third parties;
      \item manage all media related activities, which may include, but is not
        limited to, managing and updating the Website, Facebook Page, and
        Bearsden Page, and overseeing the design, printing and distribution of
        posters, brochures and other advertisements;
      \item conduct or oversee technical administration of entities including,
        but not limited to, the Website, the Facebook Page, and the Bearsden
        Page; and
      \item take meeting minutes.
    \end{enumerate}
  \item Graduate Students' Association (GSA) Representative\\
    It is the responsibility of the GSA Representative to:
    \begin{enumerate}
      \item attend GSA Council meetings and vote in the interests of the physics
        graduate students;
      \item communicate relevant information to Council or full members, as
        appropriate, in a timely manner; and
      \item appoint an alternate to act as temporary GSA representative from the
        Council, in case of absence.
    \end{enumerate}
  \item Focus Area Representatives\\
    It is the responsibility of focus area representatives to:
    \begin{enumerate}
      \item act as liaisons between the graduate students in their focus area
        and Council by relaying concerns of the students to Council and passing
        on important information from Council, or any other body, to the
        students;
      \item make strong efforts to know all graduate students in their focus
        area personally and maintain personal relations;
      \item maintain a list of names, offices and email addresses of
        all graduate students in their focus area;
      \item collect input on and help interview candidates for faculty positions
        within the Department of Physics;
      \item help to find students to sit on various committees, if necessary;
        and
      \item assist Council with any other tasks, as required.
    \end{enumerate}
    There shall be one focus area representative for each of:
    \begin{enumerate}
      \item Astrophysics and Space Physics
      \item Condensed Matter Physics
      \item Geophysics
      \item Particle Physics
    \end{enumerate}
\end{enumerate}

\subsection{Affiliated Council Members}
The Council can invite full members of the GPSA to become Affiliated Council
Members.  Invited full members can decline the invitation. Affiliated Council
Members can resign from their position at any time. It is the responsibility of
Affiliated Council Members to:
\begin{enumerate}
  \item make reasonable efforts to attend Council meetings; and
  \item assist Council with the general running of the GPSA, as required.
\end{enumerate}
Affiliated Council Members cannot vote on decisions made by the Council.

\subsection{Succession}
\label{sec:Succession}
The Council, as elected under section~\ref{sec:Election of Council}, will assume
their positions on January 1. In the event that any member of Council resigns or
is impeached during their tenure, a temporary replacement will be appointed by
Council until a by-election is held. The by-election will be held within a
reasonable time period following the resignation or removal from office of any
Council member. A resigning Council member is strongly encouraged to suggest a
replacement candidate.

\subsection{Term of Office}
\label{sec:Term of Office}
The term of office of each elected officer is one year. The term starts on
January 1 and ends on December 31 each year.

\subsection{Remuneration}
Council members will not receive any form of remuneration for their service.

\subsection{Council Meetings}
Council meetings may be organized by Council on an ad hoc basis. All Council
members must make all reasonable efforts to attend.

\subsection{Approval Process}
Any decision put before Council must be approved by a simple majority of
council. The President will decide the vote in the event of a tie.

\subsection{Financial Review}
\label{sec:Financial Review}
A review of the GPSA's finances will be conducted following the end of the
fiscal year, which is defined as December 31, and will be performed by the
outgoing Vice President Finance and one additional GPSA member, as appointed by
Council. The purpose of the review is to ensure that all transactions carried
out over the past year during the outgoing Vice President Finance's tenure are
correctly accounted for and to assess the financial standing of the GPSA. A
financial report will be submitted to Council prior to its succession.



\section{Meetings and Elections}
\subsection{Annual General Meeting}
\label{sec:Annual General Meeting}
An annual general meeting must be held in November, with the following
requirements:
\begin{enumerate}
  \item The event must be advertised to all full members with a minimum of one
    week's notice via the Mailing List;
  \item The President must submit and discuss a report outlining work done
    during the previous year and upcoming plans, and the Vice President Finance
    must submit and discuss a report outlining the financial standing of the
    GPSA; and
  \item Council members must make all reasonable efforts to attend and a minimum
    of one Council member must attend.
\end{enumerate}

\subsection{Election of Council}
\label{sec:Election of Council}
An election must be held in November to elect the officers that will comprise
Council for the forthcoming calendar year, with the following requirements:
\begin{enumerate}
  \item The election must be advertised to all full members with a minimum of
    one week's notice via the Mailing List;
  \item Officer positions will be voted for independently in order of
    precedence, as per section~\ref{sec:Elected Officers};
  \item Voting will proceed as per section~\ref{sec:Voting};
  \item For each position, the winner from all candidates is determined
    according to an instant run-off scheme, as follows:
    \begin{enumerate}
      \item Candidates will be displayed in random order on a per-ballot basis
      \item ``None of the others'' must be a possible choice
      \item Voters rank any number of candidates numerically
      \item The number of first-place votes for each candidate is counted
      \item A candidate with more than half of the votes wins
      \item If no candidate has more than half of the votes, the candidate with
        the fewest votes is eliminated
      \item Ballots for the eliminated candidate are recounted, the vote being
        redistributed to the next-highest ranked remaining candidate for each
        ballot
      \item Candidates are eliminated until a candidate supported by more than
        half of the votes is found
      \item If ``None of the others'' wins, the position will be left vacant
    \end{enumerate}
\end{enumerate}
By-elections will follow the same election procedure, as outlined above.

\subsection{Emergency Impeachment Meetings}
Should any full member of the GPSA wish to impeach a Council member, meetings
with the Physics Graduate Students Advisor must first be held to attempt to
rectify the issue. If these meetings fail to resolve the issue, then the
following criteria must be satisfied:
\begin{enumerate}
  \item The emergency impeachment meeting (EIM) must be advertised to all full
    members with a minimum of one week's notice via the Mailing List;
  \item A chairperson must be nominated and elected at the beginning of the EIM
    by a vote carried by a majority of no less than two thirds of the full
    members in attendance, excluding the person facing impeachment, as per
    section~\ref{sec:Voting};
  \item A secretary must be appointed by those members of Council in attendance
    not facing impeachment to record the discussion and outcome of the
    impeachment vote;
  \item The Physics Graduate Students Advisor, if available, or another member
    of the Department of Physics Council, must be requested to attend the EIM;
  \item Any full member in attendance may suggest any specific penalty other
    than or in addition to impeachment for the Council member facing
    impeachment; and
  \item Impeachment, or any other specific penalties, must be decided by a vote
    carried by a majority of no less than two thirds of the full members in
    attendance, excluding the person facing impeachment, respective of quorum,
    as per section~\ref{sec:Quorum}.
\end{enumerate}

\subsection{Quorum}
\label{sec:Quorum}
Quorum is set at 15\% of the number of full members for all voting by full
members. If the quorum is not satisfied then the ballot is invalid and
must be repeated.

\subsection{Voting}
\label{sec:Voting}
A vote by full members must be done in such a way that the identity of the voter
remains secret. Each full member may vote only once. The voting will normally be
conducted online, except in the case of considerable impracticality. The ballot
must be advertised to all full members with a minimum of one week's notice via
the Mailing List and has to remain open for a minimum of one week before the
vote is tallied. The results of the ballot must be made known via the Mailing
List.\\

\noindent The President, or an alternate, free from conflict of interest, will
oversee and organize the voting process and must write and submit a report
detailing the voting process and the results.\\

\noindent If the ballot, or part of the ballot, is inconclusive (in the event of
a tie, for example, or if the ballot does not satisfy quorum as outlined in
section~\ref{sec:Quorum}), the ballot or the relevant part of the ballot shall
be repeated, following the procedure as outlined in this section.



\section{Constitutional Amendments}
Any changes to the Constitution must be approved by a vote carried by a majority
of no less than two thirds of all full members voting, respective of quorum, as
per section~\ref{sec:Quorum}. Voting will proceed as per
section~\ref{sec:Voting}. The proposed changes to the Constitution must be made
known to all full members with a minimum of one week's notice before voting
commences via the Mailing List. A prior general meeting to announce and discuss
the amendments is recommended. Any full member may suggest changes to the
Constitution, but such changes will only go forward if approved by Council.



\section{Constitution Transition 2013}
\label{sec:Constitution Transition 2013}
Revision 1.4 of the Constitution comes into effect in 2013, replacing the old
Constitution, revision 1.3. For 2013, the Election of Council as outlined in
section~\ref{sec:Election of Council} will take place in March, not November.
Additionally, the terms of office of all elected officers will start on April 1,
2013 and end on December 31, 2013, overriding the periods as outlined in
sections \ref{sec:Succession} and \ref{sec:Term of Office}, for the year 2013
only. The election for the next Council, taking office on January 1, 2014, will
take place in November 2013, according to section~\ref{sec:Election of Council}.
The financial review for 2012/2013 will follow section~\ref{sec:Financial
Review}, but will take place within a reasonable time period after March 31,
2013. This section of the Constitution (section~\ref{sec:Constitution Transition
2013}) will become ineffective on January 1, 2014.

\end{document}
