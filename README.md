# GPSA Constitution

The Constitution of the [Graduate Physics Student
Association](http://gpsa.physics.ualberta.ca/) at the [University of
Alberta](http://ualberta.ca/). The Constitution is now maintained as a Latex
document. The Git repository is at <https://bitbucket.org/meznom/gpsa_constitution>.

March 2013, Burkhard Ritter <burkhard@ualberta.ca>
